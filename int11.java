class parent{

	parent(){
	
		System.out.println("in parent constructor");
	}

}
class child extends parent{

	child(){
	
		System.out.println("in child constructor");
	}
}
class client{

	public static void main(String args[]){
	
		parent obj1=new parent();
		child obj2=new child();
	}
}
