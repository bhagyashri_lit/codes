class demo{

	public static void main(String args[]){
	
		int row=4;
		int count=1;
		char ch='a';
		for(int i=1;i<=row;i++){
		
			for(int j=1;j<=row;j++){
			
				if(i%2==1){
				
					System.out.print(count++ +" ");
				}
				else{
				
					System.out.print(ch++ +" ");
				}
			
			}
			System.out.println();
		}
	}
}
