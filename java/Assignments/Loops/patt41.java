/*
 *
 *     1
 *   1 2 1
 * 1 2 3 2 1
 *
 *
 *
 *
 */

class demo{

	public static void main(String[] args){


		int row=3;
		for(int i=1;i<=row;i++){

			int num=1;
		
			for(int j=1;j<=row-i;j++){
			
				System.out.print("\t");
			}
			for(int j=1;j<=((i-1)*2)+1;j++){
				
				if(j<i)
					System.out.print(num++ +"\t");
				else
					System.out.print( num-- +"\t"); 
			}
			System.out.println();
		}
	}
}
